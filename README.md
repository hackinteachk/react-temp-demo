# Temperature conversion demo with React


This repo is created for basic understanding of React concept via simple component extracting and some function calls.

---

### Run the app

```
>>> yarn start
```

---

### How it works

Basically the server would find any file with **index** name with any or some extensions like **php, html, js**. 

Look at `public/index.html`, the only tag in `<body>...</body>` is `<div id='root'>/<div>` with nothing even inside the `div` tag. 

Let move to `src/index.js`, this is where we inject to **React** `<App/>` **component** into the `div` tag with the id  `root` (`document.getElementById('root')`. The crucial part of this file is that `index.js` must live inside `/src` or root directory of the server so that it can find easily. Although there might be some ways to not doing this, but let just skip that for now :)

##### More about rendering [here](https://reactjs.org/docs/rendering-elements.html)

Now, inside `src/js`, you'll find some `js` files. The main part of this app is within 2 files : `TempCalculator.js` and `TempInput.js`. *TempCalculator* is the one who calls *TempInput*, the guys who read input from user. 

#### Just to be precise

```
render() { 
.
.
.
return (
...
<TemperatureInput
    scale="c"
    temperature={celsius}
    onTemperatureChange={this.handleCelsiusChange}
/>
.
.
.
)}
```

See the `scale`, `temperature`, `onTemperatureChange` attributes in the input tag ? those are called **props** in react. 

##### You can read more about props [here](https://reactjs.org/docs/components-and-props.html)

Note: to use `TemperatureInput` outside its file, you must declare `export default <class_name>` some where in that file else, it won't works!.

Lastly, file `App.js` which is called by `index.js` rendered only on thing, `TempCalculator`. Then the `index.js` called the `App` component and rendered them all (`TemperatureInput`, `TempCalculator`, `App`).

If you look at the code inspector of the browser, you'd see that the `<div id='root>` tag is NOT empty anymore! rather, the `<App>` tag lived inside the `<div>` and the rest are nested until the end. You can see it yourself.

## NOTE 

Pls read [this](https://reactjs.org/docs/hello-world.html) docs (Quick start section) to see the point.

This repo is came from the lifting state up part in the docs above, writing it neatly (may be?).

This is a brief concept of how react works. It might be wrong in some part though, just check it out later on :)

-- Hackinteach K.