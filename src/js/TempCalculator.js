import React, {Component} from 'react'
import TemperatureInput from './TempInput'

function toCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
}

function toFahrenheit(celsius) {
    return (celsius * 9 / 5) + 32;
}

function tryConvert(temperature, convert) {
    const input = parseFloat(temperature);
    if (Number.isNaN(input)) {
        return '';
    }
    const output = convert(input);
    const rounded = Math.round(output * 1000) / 1000;
    return rounded.toString();
}

class Calculator extends Component {

    constructor(props){
        super(props);
        this.handleCelsiusChange = this.handleCelsiusChange.bind(this);
        this.handleFahrenheitChange = this.handleFahrenheitChange.bind(this);
        this.state = {
            temperature: '',
            scale:'c',
        };

    }

    handleFahrenheitChange(temperature){
        this.setState({
            scale: 'f',
            temperature
        });
    }
    handleCelsiusChange(temperature){
        this.setState({
            scale: 'c',
            temperature
        });
    }

    render() {
        // const temperature = this.state.temperature;
        const temperature = this.state.temperature;
        const scale = this.state.scale;
        const celsius = scale==='c' ? temperature : tryConvert(temperature,toCelsius);
        const fahrenheit = scale==='f' ? temperature : tryConvert(temperature,toFahrenheit);

        return (
            <div>
                <TemperatureInput
                    scale="c"
                    temperature={celsius}
                    onTemperatureChange={this.handleCelsiusChange} />

                <TemperatureInput
                    scale="f"
                    temperature={fahrenheit}
                    onTemperatureChange={this.handleFahrenheitChange} />
            </div>
        );
    }
}

export default Calculator;