import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './js/App';
import registerServiceWorker from './js/registerServiceWorker';

/*
* Note the directory path of index.js and other js files.
* index.js must be placed at root ('src') folder but other
* can be placed anywhere else, just use them appropriately.
* -- Nut
* */
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
